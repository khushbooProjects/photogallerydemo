//
//  urlModel.swift
//  PhotoGallery
//
//  Created by KP on 11/03/22.
//

import Foundation

// MARK: - Urls
struct Urls: Codable {
    var raw, full, regular, small: String?
    var thumb, smallS3: String?

    enum CodingKeys: String, CodingKey {
        case raw, full, regular, small, thumb
        case smallS3 = "small_s3"
    }
}


