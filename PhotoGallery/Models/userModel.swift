//
//  userModel.swift
//  PhotoGallery
//
//  Created by KP on 11/03/22.
//

import Foundation

// MARK: - User
struct User: Codable {
    var id: String?
    var username, name, firstName, lastName: String?

    enum CodingKeys: String, CodingKey {
        case id
        case username, name
        case firstName = "first_name"
        case lastName = "last_name"
    }
}
