//
//  PhotoModel.swift
//  PhotoGallery
//
//  Created by KP on 11/03/22.
//

import Foundation

// MARK: - PhotoModel
struct PhotoModel: Codable {
    var id: String?
    var createdAt, updatedAt: String?
    var photoModelDescription: String?
    var urls: Urls?
    var user: User?

    enum CodingKeys: String, CodingKey {
        case id
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case photoModelDescription = "description"
        case urls, user
    }
}

