//
//  UIImageView+Extension.swift
//  PhotoGallery
//
//  Created by KP on 11/03/22.
//

import Foundation
import UIKit
import SDWebImage

extension UIImageView {
    func setImageFromServer(urlString:String?) {
        var placeimg = UIImage(named: "user_placeholder1")
        var color = SDWebImageActivityIndicator.gray
        if let urlStr = urlString, let urlofStr = URL(string: urlStr) {
            self.sd_imageIndicator = color
            self.sd_setImage(with: urlofStr)
        }
    }
}
