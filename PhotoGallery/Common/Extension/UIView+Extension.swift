//
//  UIView+Extension.swift
//  PhotoGallery
//
//  Created by KP on 11/03/22.
//

import Foundation
import UIKit

extension UIView {
    func cornerRadius(radius:CGFloat = 15.0) {
        if #available(iOS 11.0, *) {
            self.clipsToBounds = true
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner,.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }else{
            self.layer.cornerRadius = radius
            self.layer.masksToBounds = true
        }
        
        
    }
    
   
}
