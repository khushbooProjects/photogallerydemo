//
//  UICollectionView+Extension.swift
//  PhotoGallery
//
//  Created by KP on 11/03/22.
//

import Foundation
import UIKit

extension UICollectionView {
    func restore() {
        self.backgroundView = nil
    }
    
    func addLoaderView() {
        let frame = self.frame
        let loader = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        self.center = loader.center
        loader.startAnimating()
        loader.color = UIColor.black
        self.backgroundView = loader;
        self.frame = frame
    }
}
