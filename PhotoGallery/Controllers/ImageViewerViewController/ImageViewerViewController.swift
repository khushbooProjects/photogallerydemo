//
//  ImageViewerViewController.swift
//  PhotoGallery
//
//  Created by KP on 11/03/22.
//

import UIKit

class ImageViewerViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var viewWrapper: UIView!

    var imageView: UIImageView!
    var passedImageUrl : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        let vWidth = UIScreen.main.bounds.width
        let vHeight = UIScreen.main.bounds.height

        let scrollImg: UIScrollView = UIScrollView()
        scrollImg.delegate = self
        scrollImg.frame = CGRect(x: 0, y: 0, width: vWidth, height: vHeight)
        scrollImg.alwaysBounceVertical = false
        scrollImg.alwaysBounceHorizontal = false
        scrollImg.showsVerticalScrollIndicator = true
        scrollImg.flashScrollIndicators()

        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0

        viewWrapper!.addSubview(scrollImg)
        
        imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: vWidth, height: vHeight))
        imageView.setImageFromServer(urlString: passedImageUrl)
        imageView.contentMode = .scaleAspectFit
        scrollImg.addSubview(imageView)

    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

}
