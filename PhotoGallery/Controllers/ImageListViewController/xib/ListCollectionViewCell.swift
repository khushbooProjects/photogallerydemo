//
//  ListCollectionViewCell.swift
//  PhotoGallery
//
//  Created by KP on 11/03/22.
//

import UIKit

class ListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelUsername:UILabel!
    @IBOutlet weak var labelDescription:UILabel!
    @IBOutlet weak var imageThumbnail:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageThumbnail.cornerRadius(radius: 5)
    }
    
    func fillContents(photoModel:PhotoModel) {
        self.imageThumbnail.setImageFromServer(urlString: photoModel.urls?.thumb)
        self.labelUsername.text = photoModel.user?.username
        self.labelDescription.text = photoModel.photoModelDescription
    }

}
