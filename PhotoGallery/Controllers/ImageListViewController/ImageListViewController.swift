//
//  ImageListViewController.swift
//  PhotoGallery
//
//  Created by KP on 11/03/22.
//

import UIKit

class ImageListViewController: UIViewController {

    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var buttonListGrid : UIButton!
    @IBOutlet weak var searchBar : UISearchBar!
    var isGrid = true
    var photoModelArray : [PhotoModel] = []
    var filteredPhotoModelArray : [PhotoModel] = []
    var isLoading = true
    var searchActive = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        
        self.searchBar.showsCancelButton = false
        self.searchBar.delegate = self
        buttonListGrid.addTarget(self, action: #selector(self.listGridSwitch), for: .touchUpInside)
        setupCollectionView()
    }
    
    fileprivate func setupCollectionView() {
        ["ListCollectionViewCell","GridCollectionViewCell"].forEach { (identifier) in
            collectionView.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
        }
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
        fetchImages();
    }
    
    @objc fileprivate func listGridSwitch() {
        toggleListGrid()
    }
    
    fileprivate func toggleListGrid() {
        isGrid = !isGrid
        buttonListGrid.setTitle(isGrid ? "List" : "Grid", for: .normal)
        collectionView.reloadData()
    }
    
    
    
    

}

//MARK: UICollectionView
extension ImageListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if(isLoading) {
            collectionView.addLoaderView()
            return 0;
        }else{
            collectionView.restore()
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchActive ? filteredPhotoModelArray.count : photoModelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let photoObj = searchActive ? filteredPhotoModelArray[indexPath.item] : photoModelArray[indexPath.item]
        if(isGrid) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCollectionViewCell", for: indexPath) as! GridCollectionViewCell
            cell.fillContents(photoModel: photoObj)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListCollectionViewCell", for: indexPath) as! ListCollectionViewCell
            cell.fillContents(photoModel: photoObj)
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(isGrid) {
            return CGSize(width: collectionView.frame.size.width/2, height: 200)
        }else{
            return CGSize(width: collectionView.frame.size.width, height: 100)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photoObj = searchActive ? filteredPhotoModelArray[indexPath.item] : photoModelArray[indexPath.item]
        if let imageUrl = photoObj.urls?.full {
            let controller = ImageViewerViewController(nibName: "ImageViewerViewController", bundle: nil)
            controller.passedImageUrl = imageUrl
            self.present(controller, animated: true, completion: nil)
        }
        
    }
}

//MARK: API
extension ImageListViewController {
    fileprivate func fetchImages() {
        showLoading(value: true)
        let networkManager = NetworkManager()
        networkManager.request(fromURL: urlKey.fetchImages, parameters: [authKey:authValue]) { (result: Result<[PhotoModel], Error>) in
            switch result {
            case .success(let images):
                self.showLoading(value: false)
                self.photoModelArray = images;
            case .failure(let error):
                debugPrint("We got a failure trying to get the users. The error we got was: \(error.localizedDescription)")
            }
         }
    }
    
    fileprivate func showLoading(value:Bool) {
        isLoading = value;
        collectionView.reloadData()
    }
}

//MARK: Search

extension ImageListViewController : UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.text = nil
        searchBar.resignFirstResponder()
        self.searchBar.showsCancelButton = false
        collectionView.reloadData()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }

    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }


    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        self.searchActive = true;
        self.searchBar.showsCancelButton = true
        
        filteredPhotoModelArray = photoModelArray.filter({ photoModelObj in
            return (photoModelObj.user?.username?.lowercased().contains(searchText.lowercased()) ?? false)
        })
        
        collectionView.reloadData()
            
    }
}
